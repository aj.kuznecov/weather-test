package com.template.weathertest;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;

import com.template.weathertest.model.database.WeatherDBHelper;
import com.template.weathertest.model.database.WeatherDBImpl;
import com.template.weathertest.model.database.WeatherDb;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class WeatherDBTest {
    private WeatherDBHelper dbHelper = new WeatherDBHelper(InstrumentationRegistry.getInstrumentation().getTargetContext());

    @Test
    public void insertResponseEqualsLastResponse() {
        final String TEST_STRING = "123";

        dbHelper.insertResponseJson(TEST_STRING);
        Assert.assertEquals(dbHelper.getLastResponse(), TEST_STRING);
    }
}
