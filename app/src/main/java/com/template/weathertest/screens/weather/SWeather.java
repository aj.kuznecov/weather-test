package com.template.weathertest.screens.weather;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.template.weathertest.R;
import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;
import com.template.weathertest.screens.Screen;
import com.template.weathertest.widget.WidgetLocation;
import com.template.weathertest.widget.WidgetInfo;
import com.template.weathertest.widget.WidgetToolbar;

import java.util.Locale;

public class SWeather extends Screen implements WeatherContract.view {
    private WeatherContract.Presenter presenter;

    private WidgetToolbar widgetToolbar;
    private WidgetLocation widgetLocation;
    private WidgetInfo widgetTemperature;
    private WidgetInfo widgetHimidity;
    private WidgetInfo widgetPressure;
    private WidgetInfo widgetWind;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.screen_weather, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        widgetToolbar = view.findViewById(R.id.widget_toolbar);
        widgetLocation = view.findViewById(R.id.widget_location);
        widgetTemperature = view.findViewById(R.id.widget_temperature);
        widgetHimidity = view.findViewById(R.id.widget_himidity);
        widgetPressure = view.findViewById(R.id.widget_pressure);
        widgetWind = view.findViewById(R.id.widget_wind);

        widgetToolbar.setRefreshClickListener(v -> presenter.loadWeather());

        presenter = new WeatherPresenter(this);
    }

    @Override
    public void showErrorMessage(boolean visible, String text) {
        widgetToolbar.<TextView>findViewById(R.id.error_text).setText(text);
        widgetToolbar.<TextView>findViewById(R.id.error_text).setVisibility(visible? View.VISIBLE: View.GONE);
    }

    //todo: use string resources
    @Override
    public void showWeather(WeatherResponse weatherResponse) {
        if(weatherResponse == null){
            return;
        }
        widgetLocation.setTitle(weatherResponse.name);
        widgetLocation.setCoords(weatherResponse.coord.lat, weatherResponse.coord.lon);

        String temperature = String.format(Locale.ENGLISH, "%.0f", weatherResponse.main.temp);
        widgetTemperature.setValue(temperature + "°C");
        widgetTemperature.setTitle("TEMPERATURE");
        widgetTemperature.setSubtitle(String.format(Locale.ENGLISH, "min %.0f°C  max %.0f°C", weatherResponse.main.tempMin, weatherResponse.main.tempMax));


        String himidity = String.format(Locale.ENGLISH, "%d", weatherResponse.main.humidity);
        widgetHimidity.setValue(himidity + "%");
        widgetHimidity.setTitle("HIMIDITY");

        float mmHg = weatherResponse.main.pressure / 1.33322f;//convert from hPa to mmHg
        widgetPressure.setValue(String.format(Locale.ENGLISH, "%.0f", mmHg));
        widgetPressure.setSubtitle("mmHg");
        widgetPressure.setTitle("PRESSURE");

        widgetWind.setValue(String.format(Locale.ENGLISH, "%.1f", weatherResponse.wind.speed));
        widgetWind.setSubtitle("m/s");
        widgetWind.setTitle("WIND");


    }

    @Override
    public void showLoading(boolean show) {
        widgetToolbar.setLoading(show);
    }


    @Override
    public void onPause() {
        super.onPause();
        presenter.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.destroy();
    }
}
