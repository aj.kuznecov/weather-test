package com.template.weathertest.screens.weather;

import android.os.Handler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.template.weathertest.App;
import com.template.weathertest.model.WeatherApi;
import com.template.weathertest.model.database.WeatherDb;
import com.template.weathertest.model.location.LocationProvider;
import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;

public class WeatherPresenter implements WeatherContract.Presenter {
    private final WeatherContract.view view;

    private Handler handler = new Handler();

    private LocationProvider locationProvider = App.getSingletons().getLocationProvider();
    private WeatherDb weatherDb = App.getSingletons().getWeatherDb();
    private WeatherApi weatherApi = App.getSingletons().getWeatherApi();

    private boolean onceLoadedFromApi;

    public WeatherPresenter(WeatherContract.view view) {
        this.view = view;
        locationProvider.setLocationUpdateListener(this::loadWeather);

        loadLastWeather();
        loadWeather();
    }

    private void loadLastWeather() {
        weatherDb.getLastResponseJson(response -> {
            if (onceLoadedFromApi) {
                return;
            }
            if (response == null) {
                return;
            }
            try {
                WeatherResponse weatherResponse = new Gson().fromJson(response, WeatherResponse.class);
                view.showWeather(weatherResponse);
            } catch (JsonSyntaxException e) {
                view.showErrorMessage(true, e.getMessage());
            }
        });
    }

    @Override
    public void loadWeather() {
        view.showLoading(true);
        weatherApi.requestWeather(new WeatherApi.ResponseCallback() {
            @Override
            public void onResponse(WeatherResponse weatherResponse) {
                onceLoadedFromApi = true;

                view.showWeather(weatherResponse);
                view.showLoading(false);
                view.showErrorMessage(false, null);
            }

            @Override
            public void onError(String message) {
                view.showErrorMessage(true, message);
                handler.postDelayed(WeatherPresenter.this::loadWeather, 200);
            }
        });
    }


    @Override
    public void resume() {

    }

    @Override
    public void destroy() {
        handler.removeCallbacksAndMessages(null);
        locationProvider.setLocationUpdateListener(null);
    }

    @Override
    public void pause() {

    }


}
