package com.template.weathertest.screens;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.template.weathertest.App;
import com.template.weathertest.R;
import com.template.weathertest.screens.weather.SWeather;

public class SLoad extends Screen {

    private TextView infoText;
    private View buttonGrantPermissions;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.screen_load, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        infoText = view.findViewById(R.id.info_text);
        buttonGrantPermissions = view.findViewById(R.id.b_grant_permissions);

        buttonGrantPermissions.setOnClickListener(v -> openPermissionsWindow(v.getContext()));

        tryAskPermission(getActivity());
        checkPermissionsLoop();
    }

    private void tryAskPermission(Activity activity) {
        if (hasFileLocationPermission()) {
            return;
        }
        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
    }

    private boolean hasFileLocationPermission() {
        return ContextCompat.checkSelfPermission(App.getInstance(), Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void checkPermissionsLoop() {
        if (hasFileLocationPermission()) {
            nextScreen();
        } else {
            postDelayed(this::checkPermissionsLoop, 1_000);
        }
    }

    private void nextScreen() {
        getActivity()
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.screens_container, new SWeather())
                .commitAllowingStateLoss();
    }

    private void openPermissionsWindow(Context context) {
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        startActivity(intent);
    }
}
