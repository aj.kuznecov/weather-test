package com.template.weathertest.screens.weather;

import androidx.lifecycle.LifecycleObserver;

import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;

public interface WeatherContract {
    interface view{
        void showErrorMessage(boolean visible, String text);
        void showWeather(WeatherResponse weatherResponse);
        void showLoading(boolean show);
    }

    interface Presenter {
        void loadWeather();
        void pause();
        void resume();
        void destroy();
    }
}
