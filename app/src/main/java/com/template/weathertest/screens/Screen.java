package com.template.weathertest.screens;

import android.os.Handler;

import androidx.fragment.app.Fragment;

public abstract class Screen extends Fragment {
    private Handler handler = new Handler();

    public void post(Runnable runnable){
        handler.post(runnable);
    }

    public void postDelayed(Runnable runnable, long delay){
        handler.postDelayed(runnable, delay);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        handler.removeCallbacksAndMessages(null);
    }
}
