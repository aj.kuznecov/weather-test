package com.template.weathertest.utils.events;


import java.util.ArrayList;

public class ActionEvent<T> extends BaseEvent<Action<T>> {
    private ArrayList<Action<T>> callbacks = new ArrayList<>();

    public void invoke(T t){
        for(Action<T> action: new ArrayList<>(callbacks)){
            action.invoke(t);
        }
    }

    @Override
    public void addListener(Action<T> listener){
        callbacks.add(listener);
    }

    @Override
    public void removeListener(Action<T> listener){
        callbacks.remove(listener);
    }

    @Override
    public void clearCallbacks(){
        callbacks.clear();
    }
}