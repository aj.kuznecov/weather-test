package com.template.weathertest.utils.events;

public abstract class BaseEvent<T> {

    public abstract void clearCallbacks();
    public abstract void addListener(T listener);
    public abstract void removeListener(T listener);
}
