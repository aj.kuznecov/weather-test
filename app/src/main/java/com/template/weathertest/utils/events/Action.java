package com.template.weathertest.utils.events;

public interface Action<T> {
    void invoke(T t);

}
