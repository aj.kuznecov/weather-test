package com.template.weathertest.utils.events;

import java.util.ArrayList;

public class RunnableEvent extends BaseEvent<Runnable>{
    private ArrayList<Runnable> callbacks = new ArrayList<>();

    public void invoke(){
        for(Runnable r: new ArrayList<>(callbacks)){
            if(r == null)
                continue;
            r.run();
        }
    }

    @Override
    public void addListener(Runnable listener){
        callbacks.add(listener);
    }

    @Override
    public void removeListener(Runnable listener){
        callbacks.remove(listener);
    }

    @Override
    public void clearCallbacks(){
        callbacks.clear();
    }
}