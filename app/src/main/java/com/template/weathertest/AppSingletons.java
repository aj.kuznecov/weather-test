package com.template.weathertest;

import com.template.weathertest.model.WeatherApi;
import com.template.weathertest.model.database.WeatherDb;
import com.template.weathertest.model.location.LocationProvider;

public interface AppSingletons {
    WeatherApi getWeatherApi();
    LocationProvider getLocationProvider();
    WeatherDb getWeatherDb();
}
