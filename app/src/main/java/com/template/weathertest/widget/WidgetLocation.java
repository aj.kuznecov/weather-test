package com.template.weathertest.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.template.weathertest.R;

import java.util.Locale;

public class WidgetLocation extends CardView {

    private TextView title;
    private TextView coords;

    public WidgetLocation(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.widget_location, this);

        title = findViewById(R.id.title);
        coords = findViewById(R.id.coords);
    }


    public void setTitle(String string) {
        title.setText(string);
    }

    public void setCoords(float lat, float lon) {
        coords.setText(String.format(Locale.ENGLISH, "lat: %.1f  lon: %.1f", lat, lon));
    }

}
