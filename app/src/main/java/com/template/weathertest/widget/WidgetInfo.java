package com.template.weathertest.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.template.weathertest.R;

public class WidgetInfo extends CardView {

    private TextView title;
    private TextView value;
    private TextView subtitle;

    public WidgetInfo(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.widget_info, this);

        title = findViewById(R.id.title);
        subtitle = findViewById(R.id.subtitle);
        value = findViewById(R.id.value);
    }

    public void setValue(String string) {
        value.setText(string);
    }

    public void setTitle(String string) {
        title.setText(string);
    }

    public void setSubtitle(String string){
        subtitle.setText(string);
    }
}
