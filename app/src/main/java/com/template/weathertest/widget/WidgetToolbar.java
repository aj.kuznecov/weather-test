package com.template.weathertest.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;

import com.template.weathertest.R;

public class WidgetToolbar extends Toolbar {
    private ProgressBar progressBar;
    private View refreshButton;
    public WidgetToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init(){
        inflate(getContext(), R.layout.toolbar, this);

        progressBar = findViewById(R.id.progress_bar);
        refreshButton = findViewById(R.id.b_refresh);
    }

    public void setLoading(boolean isLoading){
        progressBar.setVisibility(isLoading? VISIBLE : INVISIBLE);
        refreshButton.setVisibility(isLoading? INVISIBLE : VISIBLE);
    }

    public void setRefreshClickListener(View.OnClickListener listener){
        refreshButton.setOnClickListener(listener);
    }
}
