package com.template.weathertest;

import android.app.Application;

import com.template.weathertest.model.WeatherApi;
import com.template.weathertest.model.WeatherApiImpl;
import com.template.weathertest.model.database.WeatherDBImpl;
import com.template.weathertest.model.database.WeatherDb;
import com.template.weathertest.model.location.LocationProvider;
import com.template.weathertest.model.location.LocationProviderImpl;
import com.template.weathertest.model.network.weather_api.WeatherNetworkApiImpl;

public class App extends Application {
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    public static AppSingletons appSingletons;

    public static AppSingletons getSingletons(){
        return appSingletons;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        initAppSingletons();
    }

    private void initAppSingletons(){
        LocationProviderImpl locationProvider = new LocationProviderImpl();
        locationProvider.start();
        WeatherDb weatherDb = new WeatherDBImpl();
        WeatherApi weatherApi = new WeatherApiImpl(locationProvider, new WeatherNetworkApiImpl(), weatherDb);
        appSingletons = new AppSingletons() {
            @Override
            public WeatherApi getWeatherApi() {
                return weatherApi;
            }

            @Override
            public LocationProvider getLocationProvider() {
                return locationProvider;
            }

            @Override
            public WeatherDb getWeatherDb() {
                return weatherDb;
            }
        };
    }


}
