package com.template.weathertest.model.network.weather_api;

import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.NonNull;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class WeatherNetworkApiImpl implements WeatherNetworkApi {
    private static final String API_KEY = "2458d8583c791b1fe0fc3fa7a2b488ae";
    private static final String REQUEST = "https://api.openweathermap.org/data/2.5/weather?units=metric&lat=%f&lon=%f&appid=" + API_KEY;

    private NetworkAsync currentAsyncTask;

    @Override
    public void requestWeather(Location location, @NonNull ResponseCallback callback) {
        if (currentAsyncTask != null) {
            currentAsyncTask.cancel(true);
        }
        currentAsyncTask = new NetworkAsync(callback);
        try {
            URL url = getRequestUrl(location);
            currentAsyncTask.execute(url);
        } catch (MalformedURLException e) {
            throw new AssertionError("REQUEST URL INCORRECT. Error message: " + e.getMessage());
        }
    }

    private URL getRequestUrl(Location location) throws MalformedURLException {
        String urlStr = String.format(Locale.ENGLISH, REQUEST, location.getLatitude(), location.getLongitude());
        return new URL(urlStr);
    }

}


class NetworkAsync extends AsyncTask<URL, Void, NetworkHelper.Result> {

    private WeatherNetworkApi.ResponseCallback responseCallback;

    NetworkAsync(WeatherNetworkApi.ResponseCallback responseCallback) {
        this.responseCallback = responseCallback;
    }

    @Override
    protected NetworkHelper.Result doInBackground(URL... urls) {
        NetworkHelper networkHelper = new NetworkHelper(urls[0]);
        return networkHelper.response();
    }

    @Override
    protected void onPostExecute(NetworkHelper.Result result) {
        if (result.hasError) {
            responseCallback.onError(result.errorMessage);
        } else {
            responseCallback.onResponse(result.text);
        }
    }
}


