package com.template.weathertest.model.location;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.template.weathertest.App;
import com.template.weathertest.model.location.service.LocalBinder;
import com.template.weathertest.model.location.service.LocationService;

public class LocationProviderImpl implements LocationProvider, ServiceConnection {

    private LocationService locationService;
    private Runnable locationUpdateListener;

    @Override
    public void setLocationUpdateListener(Runnable listener) {
        this.locationUpdateListener = listener;
    }

    @Nullable
    @Override
    public Location getLocation() {
        if (locationService == null) {
            return null;
        }
        return locationService.getLastLocation();
    }

    public void start() {
        bindService();
    }

    public void stop() {
        unbindService();
    }

    @Override
    public boolean isConnected() {
        return locationService != null;
    }

    private void bindService() {
        Intent intent = new Intent(App.getInstance(), LocationService.class);
        App.getInstance().bindService(intent, this, Context.BIND_AUTO_CREATE);
    }

    private void unbindService() {
        App.getInstance().unbindService(this);
    }

    private Runnable onLocationChanged = () -> {
        if (locationUpdateListener != null) {
            locationUpdateListener.run();
        }
    };

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        locationService = ((LocalBinder<LocationService>) service).getService();
        locationService.locationChangedEvent.addListener(onLocationChanged);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        locationService.locationChangedEvent.removeListener(onLocationChanged);
        locationService = null;
    }
}
