package com.template.weathertest.model.location.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.template.weathertest.App;
import com.template.weathertest.utils.events.RunnableEvent;

public class LocationService extends Service {
    private static final long MIN_TIME_UPDATE = 1000; //millis
    private static final float MIN_DISTANCE = 100f; //meters

    public final RunnableEvent locationChangedEvent = new RunnableEvent();

    private LocalBinder<LocationService> localBinder = new LocalBinder<>(this);

    private Runnable onLocationChanges = locationChangedEvent::invoke;
    private LocationListenerAdatper networkLocationListener = new LocationListenerAdatper(onLocationChanges);
    private LocationListenerAdatper gpsLocationListener = new LocationListenerAdatper(onLocationChanges);

    private LocationManager locationManager;

    private Handler handler = new Handler();

    @Override
    public void onCreate() {
        super.onCreate();
        initLoop();
    }

    private void initLoop(){
        boolean success = initLocationManager();
        if (!success) {
            handler.postDelayed(this::initLoop, 500);
        }
    }

    private boolean initLocationManager() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        if (locationManager == null) {
            Log.e(getClass().getSimpleName(), "Missing LOCATION_SERVICE");
            return false;
        }

        try {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_UPDATE, MIN_DISTANCE, gpsLocationListener);
        } catch (SecurityException e) {
            Log.e(getClass().getSimpleName(), e.getMessage() + "");
            return false;
        }

        try {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME_UPDATE, MIN_DISTANCE, networkLocationListener);
        } catch (SecurityException e) {
            Log.e(getClass().getSimpleName(), e.getMessage() + "");
            return false;
        }
        return true;
    }

    @Nullable
    public Location getLastLocation() {
        Location lastGps = gpsLocationListener.getLastLocation();
        if (locationCorrect(lastGps)) {
            return lastGps;
        }

        Location lastNetwork = networkLocationListener.getLastLocation();
        if (locationCorrect(lastNetwork)) {
            return lastNetwork;
        }

        return null;
    }

    private boolean locationCorrect(Location location) {
        return location != null && location.getLatitude() != 0.0;
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        locationChangedEvent.clearCallbacks();
        if (locationManager != null) {
            locationManager.removeUpdates(networkLocationListener);
            locationManager.removeUpdates(gpsLocationListener);
        }
        handler.removeCallbacksAndMessages(null);
    }
}
