package com.template.weathertest.model.database;

import androidx.annotation.Nullable;

public interface WeatherDb {
    void insertResponseJson(String responseJson);
    void getLastResponseJson(DBResponse<String> dbResponse);

    interface DBResponse<T>{
        void onResponse(@Nullable T t);
    }
}
