package com.template.weathertest.model.network.weather_api;

import com.template.weathertest.utils.IOUtils;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkHelper {
    private static final int DEFAULT_TIMEOUT = 5_000;

    private URL url;
    private int timeout = DEFAULT_TIMEOUT;

    public NetworkHelper(URL url) {
        this.url = url;
    }

    public void setTimeout(int timeout){
        this.timeout = timeout;
    }

    public Result response() {
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(timeout);
            connection.setReadTimeout(timeout);

            int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                return Result.Error("RESPONSE CODE IS" + responseCode);
            }

            String responseText;
            try (InputStream inputStream = connection.getInputStream()) {
                responseText = IOUtils.readText(inputStream, "utf-8");
            }
            return Result.Success(responseText);

        } catch (Exception e) {
            return Result.Error(e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    public static class Result {
        public final String text;
        public final boolean hasError;
        public final String errorMessage;

        private Result(String text, boolean hasError, String errorMessage) {
            this.text = text;
            this.hasError = hasError;
            this.errorMessage = errorMessage;
        }

        public static Result Success(String text) {
            return new Result(text, false, null);
        }

        public static Result Error(String errorMessage) {
            return new Result(null, true, errorMessage);
        }

    }
}
