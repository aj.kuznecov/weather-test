package com.template.weathertest.model.network.weather_api.response_data;

import com.google.gson.annotations.SerializedName;

public class Main {
    public float temp;
    public float pressure;
    public int humidity;
    @SerializedName("temp_min") public float tempMin;
    @SerializedName("temp_max") public float tempMax;
}
