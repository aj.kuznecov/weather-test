package com.template.weathertest.model;

import android.location.Location;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.template.weathertest.model.database.WeatherDb;
import com.template.weathertest.model.location.LocationProvider;
import com.template.weathertest.model.network.weather_api.WeatherNetworkApi;
import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;

public class WeatherApiImpl implements WeatherApi {

    private LocationProvider locationProvider;
    private WeatherNetworkApi weatherNetworkApi;
    private WeatherDb weatherDb;

    public WeatherApiImpl(LocationProvider locationProvider, WeatherNetworkApi weatherNetworkApi, WeatherDb weatherDb) {
        this.locationProvider = locationProvider;
        this.weatherNetworkApi = weatherNetworkApi;
        this.weatherDb = weatherDb;
    }


    @Override
    public void requestWeather(WeatherApi.ResponseCallback callback) {
        if (locationProvider.isConnected()) {
            Location location = locationProvider.getLocation();
            if (location == null) {
                callback.onError("Location not ready yet");
            } else {
                weatherNetworkApi.requestWeather(location, new WeatherNetworkListener(callback));
            }
        } else {
            callback.onError("service not ready");
        }
    }


    class WeatherNetworkListener implements WeatherNetworkApi.ResponseCallback {
        private WeatherApi.ResponseCallback callback;

        public WeatherNetworkListener(WeatherApi.ResponseCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onResponse(String weatherJsonResponse) {
            try {
                WeatherResponse weatherResponse = new Gson().fromJson(weatherJsonResponse, WeatherResponse.class);
                weatherDb.insertResponseJson(weatherJsonResponse);
                callback.onResponse(weatherResponse);
            } catch (JsonSyntaxException e) {
                callback.onError(e.getMessage());
            }
        }

        @Override
        public void onError(String message) {
            callback.onError(message);
        }
    }
}