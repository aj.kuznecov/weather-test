package com.template.weathertest.model.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

public class WeatherDBHelper extends SQLiteOpenHelper {
    private static final String DATABASE_FILENAME = "WeatherResponse.db";
    private static final int VERSION = 1;

    public WeatherDBHelper(@Nullable Context context) {
        super(context, DATABASE_FILENAME, null, VERSION);
    }

    public static final String CREATE_TABLE =
            "CREATE TABLE " + WeatherDBModel.TABLE_NAME + "("
                    + WeatherDBModel.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + WeatherDBModel.COLUMN_RESPONSE_JSON + " TEXT,"
                    + WeatherDBModel.COLUMN_TIMESTAMP + " INTEGER"
                    + ")";

    public void insertResponseJson(String responseJson) {
        if(responseJson == null){
            return;
        }
        try (SQLiteDatabase db = getWritableDatabase()) {
            ContentValues values = new ContentValues();
            values.put(WeatherDBModel.COLUMN_RESPONSE_JSON, responseJson);
            values.put(WeatherDBModel.COLUMN_TIMESTAMP, System.currentTimeMillis());
            db.insert(WeatherDBModel.TABLE_NAME, null, values);
        }
    }

    private static final String LATEST_QUERY = "SELECT  * FROM " + WeatherDBModel.TABLE_NAME
            + " ORDER BY " + WeatherDBModel.COLUMN_TIMESTAMP + " DESC LIMIT 1";

    @Nullable
    public String getLastResponse() {
        try (SQLiteDatabase db = getWritableDatabase();
             Cursor cursor = db.rawQuery(LATEST_QUERY, null)) {
            String result = null;
            if (cursor.moveToFirst()) {
                result = cursor.getString(cursor.getColumnIndex(WeatherDBModel.COLUMN_RESPONSE_JSON));
            }
            return result;
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + WeatherDBModel.TABLE_NAME);
        onCreate(db);
    }
}
