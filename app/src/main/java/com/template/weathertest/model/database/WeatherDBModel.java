package com.template.weathertest.model.database;

public class WeatherDBModel {
    public static final String TABLE_NAME = "WEATHER_RESPONSES";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_RESPONSE_JSON = "RESPONSE_JSON";
    public static final String COLUMN_TIMESTAMP = "TIMESTAMP";


    public final int id;
    public final String responseJson;
    public final long timestamp;

    public WeatherDBModel(int id, String responseJson, long timestamp) {
        this.id = id;
        this.responseJson = responseJson;
        this.timestamp = timestamp;
    }

}
