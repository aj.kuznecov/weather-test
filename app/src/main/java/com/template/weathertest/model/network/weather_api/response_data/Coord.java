package com.template.weathertest.model.network.weather_api.response_data;

public class Coord {
    public float lon;
    public float lat;
}
