package com.template.weathertest.model.database;

import android.os.AsyncTask;

import androidx.annotation.NonNull;

import com.template.weathertest.App;

public class WeatherDBImpl implements WeatherDb {
    private WeatherDBHelper weatherDBHelper = new WeatherDBHelper(App.getInstance());

    @Override
    public void insertResponseJson(String responseJson) {
        new DBInsertTask(weatherDBHelper, responseJson).execute();
    }

    @Override
    public void getLastResponseJson(DBResponse<String> dbResponse) {
        new DBGetLastResponseTask(weatherDBHelper, dbResponse).execute();
    }


}

class DBInsertTask extends AsyncTask<String, Void, Void> {
    private WeatherDBHelper weatherDBHelper;
    private String response;

    DBInsertTask(@NonNull WeatherDBHelper weatherDBHelper, String response) {
        this.weatherDBHelper = weatherDBHelper;
        this.response = response;
    }

    @Override
    protected Void doInBackground(String... strings) {
        weatherDBHelper.insertResponseJson(response);
        return null;
    }
}

class DBGetLastResponseTask extends AsyncTask<Void, Void, String> {

    private WeatherDBHelper weatherDBHelper;
    private WeatherDb.DBResponse<String> dbResponse;

    public DBGetLastResponseTask(@NonNull WeatherDBHelper weatherDBHelper, @NonNull WeatherDb.DBResponse<String> dbResponse) {
        this.weatherDBHelper = weatherDBHelper;
        this.dbResponse = dbResponse;
    }

    @Override
    protected String doInBackground(Void... voids) {
        return weatherDBHelper.getLastResponse();
    }

    @Override
    protected void onPostExecute(String lastResponse) {
        dbResponse.onResponse(lastResponse);
    }
}

