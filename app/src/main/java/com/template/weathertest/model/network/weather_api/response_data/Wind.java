package com.template.weathertest.model.network.weather_api.response_data;

public class Wind {
    public float speed;
    public float deg;
}
