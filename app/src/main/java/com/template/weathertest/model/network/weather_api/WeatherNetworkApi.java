package com.template.weathertest.model.network.weather_api;

import android.location.Location;

import androidx.annotation.NonNull;

import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;

public interface WeatherNetworkApi {
    void requestWeather(@NonNull Location location, ResponseCallback callback);

    interface ResponseCallback {
        void onResponse(String weatherRawJson);
        void onError(String message);
    }
}
