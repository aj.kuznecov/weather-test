package com.template.weathertest.model.network.weather_api.response_data;

public class Sys {
    public int type;
    public int id;
    public double message;
    public String country;
    public int sunrise;
    public int sunset;
}