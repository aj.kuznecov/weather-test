package com.template.weathertest.model.location.service;

import android.location.Location;
import android.os.Bundle;

import androidx.annotation.NonNull;

import com.template.weathertest.utils.events.RunnableEvent;

public class LocationListenerAdatper implements android.location.LocationListener {

    private Location lastLocation;
    private Runnable locationChangedListener;

    public LocationListenerAdatper(@NonNull Runnable locationChangedListener){
        this.locationChangedListener = locationChangedListener;
    }

    public Location getLastLocation(){
        return lastLocation;
    }

    @Override
    public void onLocationChanged(Location location) {
        lastLocation = location;
        locationChangedListener.run();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //do nothing
    }

    @Override
    public void onProviderEnabled(String provider) {
        //do nothing
    }

    @Override
    public void onProviderDisabled(String provider) {
        //do nothing
    }
}
