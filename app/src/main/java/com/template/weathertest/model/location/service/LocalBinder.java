package com.template.weathertest.model.location.service;

import android.app.Service;
import android.os.Binder;

public class LocalBinder<T extends Service> extends Binder {
    private T service;
    public LocalBinder(T service){
        this.service = service;
    }

    public T getService(){
        return service;
    }
}
