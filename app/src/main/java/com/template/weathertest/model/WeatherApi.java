package com.template.weathertest.model;

import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;

public interface WeatherApi {
    void requestWeather(ResponseCallback callback);

    interface ResponseCallback {
        void onResponse(WeatherResponse weatherResponse);
        void onError(String message);
    }
}
