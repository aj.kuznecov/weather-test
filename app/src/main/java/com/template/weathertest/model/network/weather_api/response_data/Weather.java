package com.template.weathertest.model.network.weather_api.response_data;

public class Weather {
    public int id;
    public String main;
    public String description;
    public String icon;
}
