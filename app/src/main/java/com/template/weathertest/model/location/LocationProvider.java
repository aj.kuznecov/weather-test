package com.template.weathertest.model.location;

import android.location.Location;

import androidx.annotation.Nullable;

public interface LocationProvider {
    void setLocationUpdateListener(Runnable listener);
    @Nullable
    Location getLocation();

    boolean isConnected();
}
