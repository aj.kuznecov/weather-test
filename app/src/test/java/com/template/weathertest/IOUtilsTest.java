package com.template.weathertest;

import com.template.weathertest.utils.IOUtils;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;

public class IOUtilsTest {
    @Test
    public void readCorrect() throws IOException {
        test("123", "UTF-8");
        test("1egrsedargetf", "ASCII");
    }

    private void test(String string, String encoding) throws IOException {
        try(InputStream inputStream = new ByteArrayInputStream(string.getBytes(Charset.forName(encoding)))) {
            String text= IOUtils.readText(inputStream, encoding);
            Assert.assertEquals(text, string);
        }
    }
}
