package com.template.weathertest;

import android.location.Location;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.template.weathertest.model.WeatherApi;
import com.template.weathertest.model.WeatherApiImpl;
import com.template.weathertest.model.database.WeatherDb;
import com.template.weathertest.model.location.LocationProvider;
import com.template.weathertest.model.network.weather_api.WeatherNetworkApi;
import com.template.weathertest.model.network.weather_api.response_data.Sys;
import com.template.weathertest.model.network.weather_api.response_data.WeatherResponse;
import com.template.weathertest.screens.weather.WeatherContract;
import com.template.weathertest.screens.weather.WeatherPresenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockSettings;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

@RunWith(JUnit4.class)
public class WeatherApiTest {


    @Mock private LocationProvider locationProvider;
    @Mock private WeatherDb weatherDb;

    private WeatherNetworkApi networkApi = (location, callback) -> callback.onResponse("");

    private WeatherApiImpl weatherApi;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);

        Mockito.when(locationProvider.isConnected()).thenReturn(true);
        Mockito.when(locationProvider.getLocation()).thenReturn(new Location(""));


        weatherApi = new WeatherApiImpl(locationProvider, networkApi, weatherDb);

    }

    @Test
    public void weatherApiResponse(){
        WeatherApi.ResponseCallback callback = Mockito.mock(WeatherApi.ResponseCallback.class);
        weatherApi.requestWeather(callback);
        Mockito.verify(callback).onResponse(Mockito.any());
    }
}
